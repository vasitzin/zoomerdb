file(GLOB_RECURSE CORE "*.c*" "*.h*")

add_library(ZoomerCore SHARED ${CORE})
target_link_libraries(ZoomerCore ZoomerExtra ${LIBRARIES})
target_compile_definitions(ZoomerCore PUBLIC ${DEFINITIONS})
set_target_properties(ZoomerCore PROPERTIES VERSION ${PROJECT_VERSION})
