#pragma once

#include <string>
#include <vector>
#include "../../modules/zdb/zdb.hpp"
#include "../utils/messages.hpp"
#include "../utils/storage.hpp"

namespace fs = std::filesystem;

enum operations {
  do_nothing,
  do_load,
  do_reload,
  do_unload,
  do_duplicate,
  do_add_table,
  do_rename_table,
  do_remove_table,
  do_add_header,
  do_set_header,
  do_remove_header,
  do_add_entry,
  do_set_value,
  do_move_entry,
  do_remove_entry
};

class Controller {
private:
  struct LoadStoredContainer {
    bool set{false};
    size_t stored_index{0};
  };
  struct ReloadContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
  };
  struct UnloadContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
  };
  struct DuplicateContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
  };
  struct AddTableContainer {
    bool set{false};
    zdb_title_t temp_title;
    zdb_entry_t temp_vec;
    ZDB new_table = ZDB(temp_title, temp_vec);
  };
  struct RenameTableContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
    zdb_title_t new_title{""};
  };
  struct RemoveTableContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
  };
  struct AddHeaderContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
    zdb_header_t new_header{""};
  };
  struct SetHeaderContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
    size_t header_index{0};
    zdb_header_t new_header{""};
  };
  struct RemoveHeaderContainer {
    bool set{false};
    zdb_uuid_t loaded_uuid{""};
    size_t header_index{0};
  };
  struct AddEntryContainer {
    bool set{false};
    zdb_entry_t new_entry{};
  };
  struct SetValueContainer {
    bool set{false};
    size_t row{0};
    size_t col{0};
    zdb_value_t new_value{""};
  };
  struct MoveEntryContainer {
    bool set{false};
    size_t dest{0};
    size_t row{0};
  };
  struct RemoveEntryContainer {
    bool set{false};
    size_t row{0};
  };

  Storage *strg;
  LoadStoredContainer load_stored_c{};
  ReloadContainer reload_c{};
  UnloadContainer unload_c{};
  DuplicateContainer duplicate_c{};
  AddTableContainer add_table_c{};
  RenameTableContainer rename_table_c{};
  RemoveTableContainer remove_table_c{};
  AddHeaderContainer add_header_c{};
  SetHeaderContainer set_header_c{};
  RemoveHeaderContainer remove_header_c{};
  AddEntryContainer add_entry_c{};
  SetValueContainer set_value_c{};
  MoveEntryContainer move_entry_c{};
  RemoveEntryContainer remove_entry_c{};
  operations operation{do_nothing};

public:
  Controller(){};
  Controller(Storage *strg) { this->strg = strg; };
  operations get_operation() { return this->operation; };

  void store_zdb(std::string &file_path, std::vector<StoredZDB> *stored) {
    std::ifstream file;
    fs::directory_entry entry{file_path};
    std::string title{};
    file.open(entry.path());
    ZDB::read_title(file, &title);
    stored->push_back({entry.path().filename().string(), title});
    file.close();
  };

  void load_zdb(std::string &file_path, std::vector<LoadedZDB> *loaded, bool visible = false) {
    std::ifstream file;
    fs::directory_entry entry{file_path};
    file.open(entry.path());
    loaded->push_back({entry.path().filename().string(), ZDB(file), visible});
    file.close();
  };

  void load_stored_container(size_t stored_index) {
    LoadStoredContainer *container{&this->load_stored_c};
    container->set = true;
    container->stored_index = stored_index;
    this->operation = do_load;
  };
  void load_stored(std::vector<StoredZDB> *stored, std::vector<LoadedZDB> *loaded) {
    if (!this->load_stored_c.set) throw "load_stored() 'container' not initialized!";
    std::string file_path{strg->paths.data_parent + strg->paths.seperator +
                          stored->at(this->load_stored_c.stored_index).dbfile};
    load_zdb(file_path, loaded, true);
    stored->erase(stored->begin() + this->load_stored_c.stored_index);
    this->load_stored_c = LoadStoredContainer{};
    this->operation = do_nothing;
  };

  void reload_container(zdb_uuid_t loaded_uuid) {
    ReloadContainer *container{&this->reload_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    this->operation = do_reload;
  };
  void reload(std::vector<LoadedZDB> *loaded) {
    if (!this->reload_c.set) throw "reload() 'container' not initialized!";
    size_t i{find(this->reload_c.loaded_uuid, loaded)};
    switch (loaded->at(i).table.get_state()) {
    case safe:
    case edited: {
      std::cout << "[DATA] Reloading Table..." << std::endl;
      std::string file_path{strg->paths.data_parent + strg->paths.seperator + loaded->at(i).dbfile};
      fs::directory_entry entry{file_path};
      loaded->erase(loaded->begin() + i);
      if (ZDB::is_zdb(entry)) load_zdb(file_path, loaded);
      break;
    }
    case fresh: {
      std::cout << "[DATA] Unloading newly created Table." << std::endl;
      loaded->erase(loaded->begin() + i);
      break;
    }
    }
    this->reload_c = ReloadContainer{};
    this->operation = do_nothing;
  };

  void unload_container(zdb_uuid_t loaded_uuid) {
    UnloadContainer *container{&this->unload_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    this->operation = do_unload;
  };
  zdb_uuid_t unload(std::vector<StoredZDB> *stored, std::vector<LoadedZDB> *loaded) {
    if (!this->unload_c.set) throw "unload() 'container' not initialized!";
    size_t i{find(this->unload_c.loaded_uuid, loaded)};
    zdb_uuid_t removed_uuid{loaded->at(i).table.get_uuid()};
    switch (loaded->at(i).table.get_state()) {
    case safe:
    case edited: {
      std::cout << "[DATA] Unloading Table..." << std::endl;
      std::string file_path{strg->paths.data_parent + strg->paths.seperator + loaded->at(i).dbfile};
      std::cout << file_path << std::endl;
      fs::directory_entry entry{file_path};
      loaded->erase(loaded->begin() + i);
      if (ZDB::is_zdb(entry)) store_zdb(file_path, stored);
      break;
    }
    case fresh: {
      std::cout << "[DATA] Unloading newly created Table." << std::endl;
      loaded->erase(loaded->begin() + i);
      break;
    }
    }
    this->unload_c = UnloadContainer{};
    this->operation = do_nothing;
    return removed_uuid;
  };

  void duplicate_container(zdb_uuid_t loaded_uuid) {
    DuplicateContainer *container{&this->duplicate_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    this->operation = do_duplicate;
  };
  void duplicate(std::vector<LoadedZDB> *loaded) {
    if (!this->duplicate_c.set) throw "duplicate() 'container' not initialized!";
    size_t i{find(this->duplicate_c.loaded_uuid, loaded)};
    loaded->push_back({"", ZDB::duplicate(loaded->at(i).table), false});
    this->duplicate_c = DuplicateContainer{};
    this->operation = do_nothing;
  };

  void add_table_container(ZDB new_table) {
    AddTableContainer *container{&this->add_table_c};
    container->set = true;
    container->new_table = new_table;
    this->operation = do_add_table;
  };
  void add_table(std::vector<LoadedZDB> *wraps) {
    if (!this->add_table_c.set) throw "add_table() 'container' not initialized!";
    wraps->push_back({"", this->add_table_c.new_table, true});
    this->add_table_c = AddTableContainer{};
    this->operation = do_nothing;
  };

  void rename_table_container(zdb_uuid_t loaded_uuid, zdb_title_t new_title) {
    RenameTableContainer *container{&this->rename_table_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    container->new_title = new_title;
    this->operation = do_rename_table;
  };
  void rename_table(std::vector<LoadedZDB> *loaded) {
    if (!this->rename_table_c.set) throw "rename_table() 'container' not initialized!";
    size_t i{find(this->rename_table_c.loaded_uuid, loaded)};
    loaded->at(i).table.set_title(this->rename_table_c.new_title);
    this->rename_table_c = RenameTableContainer{};
    this->operation = do_nothing;
  };

  void remove_table_container(zdb_uuid_t loaded_uuid) {
    RemoveTableContainer *container{&this->remove_table_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    this->operation = do_remove_table;
  };
  void remove_table(std::vector<LoadedZDB> *loaded) {
    if (!this->remove_table_c.set) throw "remove_table() 'container' not initialized!";
    size_t i{find(this->remove_table_c.loaded_uuid, loaded)};
    switch (loaded->at(i).table.get_state()) {
    case safe:
    case edited: {
      std::cout << "[DATA] Deleting Table..." << std::endl;
      std::string delete_path{strg->paths.data_parent + strg->paths.seperator +
                              loaded->at(i).dbfile};
      if (fs::remove(delete_path.c_str())) {
        std::cout << M_DEL_FILE_SUCCESS << std::endl;
        loaded->erase(loaded->begin() + i);
      } else
        std::cout << M_DEL_FILE_FAIL << std::endl;
      break;
    }
    case fresh: {
      std::cout << "[DATA] Newly created Table ignoring .zdb file deletion." << std::endl;
      loaded->erase(loaded->begin() + i);
      break;
    }
    }
    this->remove_table_c = RemoveTableContainer{};
    this->operation = do_nothing;
  };

  void add_header_container(zdb_uuid_t loaded_uuid, zdb_header_t new_header) {
    AddHeaderContainer *container{&this->add_header_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    container->new_header = new_header;
    this->operation = do_add_header;
  };
  void add_header(std::vector<LoadedZDB> *loaded) {
    if (!this->add_header_c.set) throw "add_header() 'container' not initialized!";
    size_t i{find(this->add_header_c.loaded_uuid, loaded)};
    if (this->add_header_c.new_header != "")
      loaded->at(i).table.add_header(this->add_header_c.new_header);
    this->add_header_c = {};
    this->operation = do_nothing;
  };

  void set_header_container(zdb_uuid_t loaded_uuid, size_t header_index, zdb_header_t new_header) {
    SetHeaderContainer *container{&this->set_header_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    container->header_index = header_index;
    container->new_header = new_header;
    this->operation = do_set_header;
  };
  void set_header(std::vector<LoadedZDB> *loaded) {
    if (!this->set_header_c.set) throw "set_header() 'container' not initialized!";
    size_t i{find(this->set_header_c.loaded_uuid, loaded)};
    if (this->set_header_c.new_header != "")
      loaded->at(i).table.set_header(this->set_header_c.new_header,
                                     this->set_header_c.header_index);
    this->set_header_c = {};
    this->operation = do_nothing;
  };

  void remove_header_container(zdb_uuid_t loaded_uuid, size_t header_index) {
    RemoveHeaderContainer *container{&this->remove_header_c};
    container->set = true;
    container->loaded_uuid = loaded_uuid;
    container->header_index = header_index;
    this->operation = do_remove_header;
  };
  void remove_header(std::vector<LoadedZDB> *loaded) {
    if (!this->remove_header_c.set) throw "remove_header() 'container' not initialized!";
    size_t i{find(this->remove_header_c.loaded_uuid, loaded)};
    loaded->at(i).table.remove_header(this->remove_header_c.header_index);
    this->remove_header_c = {};
    this->operation = do_nothing;
  };

  void add_entry_container(zdb_entry_t new_entry) {
    AddEntryContainer *container{&this->add_entry_c};
    container->set = true;
    container->new_entry = new_entry;
    this->operation = do_add_entry;
  };
  void add_entry(LoadedZDB *loaded) {
    if (!this->add_entry_c.set) throw "add_entry() 'container' not initialized!";
    loaded->table.add_entry(this->add_entry_c.new_entry);
    this->add_entry_c = {};
    this->operation = do_nothing;
  };

  void set_value_container(size_t row, size_t col, zdb_value_t new_value) {
    SetValueContainer *container{&this->set_value_c};
    container->set = true;
    container->row = row;
    container->col = col;
    container->new_value = new_value;
    this->operation = do_set_value;
  };
  void set_value(LoadedZDB *loaded) {
    if (!this->set_value_c.set) throw "set_value() 'container' not initialized!";
    loaded->table.set_value(this->set_value_c.new_value, this->set_value_c.row,
                            this->set_value_c.col);
    this->set_value_c = {};
    this->operation = do_nothing;
  };

  void move_entry_container(size_t dest, size_t row) {
    MoveEntryContainer *container{&this->move_entry_c};
    container->set = true;
    container->dest = dest;
    container->row = row;
    this->operation = do_move_entry;
  };
  void move_entry(LoadedZDB *loaded) {
    if (!this->move_entry_c.set) throw "move_entry() 'container' not initialized!";
    loaded->table.move_entry(this->move_entry_c.dest, this->move_entry_c.row);
    this->move_entry_c = {};
    this->operation = do_nothing;
  };

  void remove_entry_container(size_t row) {
    RemoveEntryContainer *container{&this->remove_entry_c};
    container->set = true;
    container->row = row;
    this->operation = do_remove_entry;
  };
  void remove_entry(LoadedZDB *loaded) {
    if (!this->remove_entry_c.set) throw "remove_entry() 'container' not initialized!";
    loaded->table.remove_entry(this->remove_entry_c.row);
    this->remove_entry_c = {};
    this->operation = do_nothing;
  };
};
