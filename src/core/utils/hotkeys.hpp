#pragma once

#include "storage.hpp"

class HotKeys {
private:
  Storage *strg;

public:
  HotKeys(){};
  HotKeys(Storage *strg) { this->strg = strg; };
  bool terminate_process() {
    strg->quit = true;
    return true;
  };
#ifdef METRICS
  bool toggle_metrics() {
    strg->panels.metrics = !strg->panels.metrics;
    return true;
  };
#endif
};
