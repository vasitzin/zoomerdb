#pragma once

#include <iostream>
#include <vector>

#define M_DATADIR             "[DATA] data parent directory: "
#define M_CONFDIR             "[INI] ZoomerDB configuration directory: "
#define M_IMGUI_CONFDIR       "[INI] Dear ImGui configuration directory: "
#define M_CREATE_DATADIR_FAIL "[DATA] The data parent directory could not be created: "
#define M_CREATE_CONFDIR_FAIL "[INI] The conf parent directory could not be created: "
#define M_CREATE_CONF_FAIL    "[INI] The configuration could not be created: "
#define M_LOAD_FILE           "[DATA] Loading file: "
#define M_FOUND_FILE          "[DATA] Found file: "
#define M_DEL_FILE_SUCCESS    "[DATA] Table File deleted successfuly."
#define M_DEL_FILE_FAIL       "[DATA] Table File deletion failed."

#define H_NEW_TABLE           "The collection of entries stored in a table format."

namespace ZoomOut {
inline void printz(const char *msg) { std::cout << msg << std::endl; };
inline void printz(std::string *msg) { std::cout << msg << std::endl; };
inline void printz(std::string *msg, std::vector<std::string> params) {
  std::cout << msg;
  for (std::string param : params) std::cout << param;
  std::cout << std::endl;
};
}; // namespace ZoomOut
