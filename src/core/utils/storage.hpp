#pragma once

#include <string>
#include <vector>
#include <GLFW/glfw3.h>
#include "../../modules/imgui/imgui.h"
#include "../../modules/zdb/zdb.hpp"

#ifndef FONTS
#define FONTS
#endif
#ifndef ICO
#define ICO
#endif
#ifndef WIDTH
#define WIDTH 854
#endif
#ifndef HEIGHT
#define HEIGHT 480
#endif
#ifndef VERSION
#define VERSION "undefined"
#endif
#ifndef FLAVOR
#define FLAVOR "unspecified"
#endif

enum launch_panels { none, session, all };

struct Directories {
  const std::string extension{".zdb"};
#ifdef __linux__
  const std::string seperator{"/"};
  std::string conf_parent{"/.config/ZoomerDB"};
  std::string data_parent{"/.local/share/ZoomerDB/data"};
  std::string ini_conf{"/conf.ini"};
  std::string ini_imgui{"/imgui.ini"};
#elif __WIN32__
  const std::string seperator{"\\"};
  std::string conf_parent{"\\AppData\\Local\\ZoomerDB\\conf"};
  std::string data_parent{"\\AppData\\Local\\ZoomerDB\\data"};
  std::string ini_conf{"\\conf.ini"};
  std::string ini_imgui{"\\imgui.ini"};
#endif
  std::string font_parent{"" FONTS};
  std::string ico_path{"" ICO};
  std::string font_ico{"fa-solid-900.ttf"};
  std::string font_default{"FreeSans.otf"};
  std::string font_italic{"FreeSansOblique.otf"};
  std::string font_mono{"JetBrainsMono-Regular.ttf"};
};

struct Config {
  int win_width{WIDTH};
  int win_height{HEIGHT};
  int win_left{1};
  int win_top{1};
  int frame_limit{1};
  int launch_panels{none};
  std::vector<zdb_uuid_t> open_panels{};
  std::vector<bool> state_panels{};
};

struct Fonts {
  ImFont *normal{nullptr};
  ImFont *italic{nullptr};
  ImFont *mono{nullptr};
};

struct Panels {
  bool info{false};
  bool create_table{false};
#ifdef METRICS
  bool metrics{false};
#endif
};

class Storage {
public:
  Storage() {};
  bool quit{false};                            // Quit the application.
  std::string version_str{"Version " VERSION}; // Application Version.
  std::string flavor_str{"" FLAVOR};           // Application OS & Backends.
  GLFWimage glfw_images[1];
  ImGuiID main_dockspace{0};
  Directories paths{};
  Config options{};
  Panels panels{};
  Fonts fonts{};
};
