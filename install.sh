#!/bin/bash

su="sudo"
cmake_type="debug"
cmake_install=true

while getopts 't:u' opt; do
  case "$opt" in
    t)
      case "$OPTARG" in
        debug | release) cmake_type="$OPTARG";;
        *) exit 99;;
      esac
      ;;
    u) cmake_install=false;;
  esac
done; shift "$(($OPTIND - 1))"
[ "$cmake_type" = "" ] && exit 99
[ -f /usr/bin/doas ] && su="doas"

cd "build/$cmake_type"

if [ "$cmake_install" = true ]; then
  echo && echo "Installing build/$cmake_type"
  $su cmake --install . && echo "Done."
else 
  echo && echo "Uninstalling build/$cmake_type"
  $su xargs rm < install_manifest.txt && echo "Done."
fi

cd ../..
