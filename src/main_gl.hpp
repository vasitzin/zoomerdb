#include <unistd.h>
#include <stdlib.h>
#include <cstdio>
#include <GLFW/glfw3.h>
#include "modules/imgui/backends/imgui_impl_opengl3.h"


#define GL_SILENCE_DEPRECATION

static void glfw_error_callback(int error, const char *description) {
  fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}
