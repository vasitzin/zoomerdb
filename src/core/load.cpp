#include "load.hpp"
#include "../modules/IconsFontAwesome6.h"
#include "utils/messages.hpp"
#include <filesystem>

#define STB_IMAGE_IMPLEMENTATION
#include "../modules/stb/stb_image.h"

/**
 * Operations to be performed before the loop at the start of 'main'.
 */
void Load::init() {
  // Set the storage format of the INI data.
  si_conf.SetUnicode();
  si_imgui.SetUnicode();

  // Configuration Directories
  {
#ifdef __linux__
    const char *home{getenv("HOME")};
#elif __WIN32__
    std::string home_str{getenv("HOMEPATH")};
    home_str = "C:" + home_str;
    const char *home{home_str.c_str()};
#endif
    if (home != NULL) {
      strg->paths.data_parent = home + strg->paths.data_parent;
      strg->paths.conf_parent = home + strg->paths.conf_parent;
    } else {
#ifdef __linux__
      strg->paths.data_parent = "./data";
      strg->paths.conf_parent = "./conf";
#elif __WIN32__
      strg->paths.data_parent = ".\\data";
      strg->paths.conf_parent = ".\\conf";
#endif
    }

    if (!std::filesystem::exists(strg->paths.data_parent))
      std::filesystem::create_directories(strg->paths.data_parent);
    assert((void(M_CREATE_DATADIR_FAIL + strg->paths.data_parent),
            std::filesystem::exists(strg->paths.data_parent)));

    std::cout << M_DATADIR << strg->paths.data_parent << std::endl;

    if (!std::filesystem::exists(strg->paths.conf_parent))
      std::filesystem::create_directories(strg->paths.conf_parent);
    assert((void(M_CREATE_CONFDIR_FAIL + strg->paths.conf_parent),
            std::filesystem::exists(strg->paths.conf_parent)));

    strg->paths.ini_conf = strg->paths.conf_parent + strg->paths.ini_conf;
    std::cout << M_CONFDIR << strg->paths.ini_conf << std::endl;
    strg->paths.ini_imgui = strg->paths.conf_parent + strg->paths.ini_imgui;
    std::cout << M_IMGUI_CONFDIR << strg->paths.ini_imgui << std::endl;
  }

  // Read configuration ini
  {
    size_t mkini_try{0};
    SI_Error rc{si_conf.LoadFile(strg->paths.ini_conf.c_str())};
    while (rc < 0 && mkini_try < 5) {
      std::ofstream conf(strg->paths.ini_conf.c_str(), std::ios::out);
      conf << "";
      conf.close();
      rc = si_conf.LoadFile(strg->paths.ini_conf.c_str());
      ++mkini_try;
    }
    assert((void(M_CREATE_CONF_FAIL + strg->paths.ini_conf), rc >= 0));

    strg->options.win_width =
        std::atoi(si_conf.GetValue("Size", "Width", std::to_string(WIDTH).c_str()));
    strg->options.win_width = (strg->options.win_width < 100) ? 100 : strg->options.win_width;
    strg->options.win_height =
        std::atoi(si_conf.GetValue("Size", "Height", std::to_string(HEIGHT).c_str()));
    strg->options.win_height = (strg->options.win_height < 100) ? 100 : strg->options.win_height;
    strg->options.win_left = std::atoi(si_conf.GetValue("Position", "Left", "10"));
    strg->options.win_top = std::atoi(si_conf.GetValue("Position", "Top", "50"));
    strg->options.frame_limit = std::atoi(si_conf.GetValue("GUI", "FrameLimit", "60"));
    strg->options.launch_panels = std::atoi(si_conf.GetValue("GUI", "LaunchPanels", "0"));
    {
      std::string panels_str = si_conf.GetValue("GUI", "OpenPanels", "");
      int end = panels_str.find(",");
      while (end != -1) {
        strg->options.open_panels.push_back(panels_str.substr(0, end));
        panels_str.erase(panels_str.begin(), panels_str.begin() + end + 1);
        end = panels_str.find(",");
      }
      strg->options.open_panels.push_back(panels_str.substr(0, end));
    }
    {
      std::string state_str = si_conf.GetValue("GUI", "StatePanels", "");
      int end = state_str.find(",");
      while (end != -1) {
        if (state_str.substr(0, end) == "1")
          strg->options.state_panels.push_back(true);
        else
          strg->options.state_panels.push_back(false);
        state_str.erase(state_str.begin(), state_str.begin() + end + 1);
        end = state_str.find(",");
      }
      if (state_str.substr(0, end) == "1")
        strg->options.state_panels.push_back(true);
      else
        strg->options.state_panels.push_back(false);
    }
  }

  // Load window icon
#ifdef APPIMG
  strg->paths.ico_path = std::filesystem::weakly_canonical(
      std::filesystem::read_symlink("/proc/self/exe").string() + strg->paths.ico_path);
#endif
  std::cout << "[ICON] Icon path: " << strg->paths.ico_path << std::endl;
  int ico_w, ico_h, ico_ch;
  unsigned char *ico_pixels = stbi_load(strg->paths.ico_path.c_str(), &ico_w, &ico_h, &ico_ch, 4);
  strg->glfw_images[0].width = ico_w;
  strg->glfw_images[0].height = ico_h;
  strg->glfw_images[0].pixels = ico_pixels;
}

/**
 * Operations to be performed before the loop to configure the imgui session.
 */
void Load::imgui_config(ImGuiIO &io) {
  (void)io;
  io.IniFilename = strg->paths.ini_imgui.c_str();       // New file path for configs
  io.ConfigViewportsNoAutoMerge = true;                 // All windows have seperate viewports
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;     // Enable Docking
  io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;   // Enable Multi-Viewport / Platform Windows

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  // ImGui::StyleColorsLight();

  // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look
  // identical to regular ones.
  ImGuiStyle &style = ImGui::GetStyle();
  auto &colors = ImGui::GetStyle().Colors;
  if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
    style.WindowRounding = 0.0f;
    colors[ImGuiCol_WindowBg] = ImVec4{0.1f, 0.1f, 0.13f, 1.0f};
    colors[ImGuiCol_MenuBarBg] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_Border] = ImVec4{0.44f, 0.37f, 0.61f, 0.29f};
    colors[ImGuiCol_BorderShadow] = ImVec4{0.0f, 0.0f, 0.0f, 0.24f};
    colors[ImGuiCol_Text] = ImVec4{1.0f, 1.0f, 1.0f, 1.0f};
    colors[ImGuiCol_TextDisabled] = ImVec4{0.5f, 0.5f, 0.5f, 1.0f};
    colors[ImGuiCol_Header] = ImVec4{0.13f, 0.13f, 0.17, 1.0f};
    colors[ImGuiCol_HeaderHovered] = ImVec4{0.19f, 0.2f, 0.25f, 1.0f};
    colors[ImGuiCol_HeaderActive] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_Button] = ImVec4{0.13f, 0.13f, 0.17, 1.0f};
    colors[ImGuiCol_ButtonHovered] = ImVec4{0.19f, 0.2f, 0.25f, 1.0f};
    colors[ImGuiCol_ButtonActive] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_CheckMark] = ImVec4{0.74f, 0.58f, 0.98f, 1.0f};
    colors[ImGuiCol_PopupBg] = ImVec4{0.1f, 0.1f, 0.13f, 0.92f};
    colors[ImGuiCol_SliderGrab] = ImVec4{0.44f, 0.37f, 0.61f, 0.54f};
    colors[ImGuiCol_SliderGrabActive] = ImVec4{0.74f, 0.58f, 0.98f, 0.54f};
    colors[ImGuiCol_FrameBg] = ImVec4{0.13f, 0.13, 0.17, 1.0f};
    colors[ImGuiCol_FrameBgHovered] = ImVec4{0.19f, 0.2f, 0.25f, 1.0f};
    colors[ImGuiCol_FrameBgActive] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_Tab] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_TabHovered] = ImVec4{0.24, 0.24f, 0.32f, 1.0f};
    colors[ImGuiCol_TabActive] = ImVec4{0.2f, 0.22f, 0.27f, 1.0f};
    colors[ImGuiCol_TabUnfocused] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_TabUnfocusedActive] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_TitleBg] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_TitleBgActive] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_TitleBgCollapsed] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_ScrollbarBg] = ImVec4{0.1f, 0.1f, 0.13f, 1.0f};
    colors[ImGuiCol_ScrollbarGrab] = ImVec4{0.16f, 0.16f, 0.21f, 1.0f};
    colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4{0.19f, 0.2f, 0.25f, 1.0f};
    colors[ImGuiCol_ScrollbarGrabActive] = ImVec4{0.24f, 0.24f, 0.32f, 1.0f};
    colors[ImGuiCol_Separator] = ImVec4{0.44f, 0.37f, 0.61f, 1.0f};
    colors[ImGuiCol_SeparatorHovered] = ImVec4{0.74f, 0.58f, 0.98f, 1.0f};
    colors[ImGuiCol_SeparatorActive] = ImVec4{0.84f, 0.58f, 1.0f, 1.0f};
    colors[ImGuiCol_ResizeGrip] = ImVec4{0.44f, 0.37f, 0.61f, 0.29f};
    colors[ImGuiCol_ResizeGripHovered] = ImVec4{0.74f, 0.58f, 0.98f, 0.29f};
    colors[ImGuiCol_ResizeGripActive] = ImVec4{0.84f, 0.58f, 1.0f, 0.29f};
    colors[ImGuiCol_DockingPreview] = ImVec4{0.44f, 0.37f, 0.61f, 1.0f};
  }

  // Load my Fonts
  imgui_font_loader(io);
}

/**
 * Save configuration
 */
void Load::cleanup() {
  si_conf.SetValue("Size", "Width", std::to_string(strg->options.win_width).c_str());
  si_conf.SetValue("Size", "Height", std::to_string(strg->options.win_height).c_str());
  si_conf.SetValue("Position", "Left", std::to_string(strg->options.win_left).c_str());
  si_conf.SetValue("Position", "Top", std::to_string(strg->options.win_top).c_str());
  si_conf.SetValue("GUI", "FrameLimit", std::to_string(strg->options.frame_limit).c_str());
  si_conf.SetValue("GUI", "LaunchPanels", std::to_string(strg->options.launch_panels).c_str());
  std::string panels_str{""};
  for (zdb_uuid_t panel : strg->options.open_panels) panels_str = panels_str + "," + panel;
  si_conf.SetValue("GUI", "OpenPanels", panels_str.c_str());
  std::string state_str{""};
  for (bool state : strg->options.state_panels) state_str = state_str + "," + std::to_string(state);
  si_conf.SetValue("GUI", "StatePanels", state_str.c_str());
  si_conf.SaveFile(strg->paths.ini_conf.c_str());
}

/**
 * Private. Load the application fonts.
 */
void Load::imgui_font_loader(ImGuiIO &io) {
#ifdef APPIMG
  strg->paths.font_parent = std::filesystem::weakly_canonical(
      std::filesystem::read_symlink("/proc/self/exe").string() + strg->paths.font_parent);
#endif
  for (fs::directory_entry e : fs::recursive_directory_iterator{strg->paths.font_parent}) {
    if (e.path().filename() == strg->paths.font_default) {
      strg->paths.font_default = e.path().string();
      std::cout << "[FONT] Default font: " << strg->paths.font_default << std::endl;
    } else if (e.path().filename() == strg->paths.font_italic) {
      strg->paths.font_italic = e.path().string();
      std::cout << "[FONT] Editing font: " << strg->paths.font_italic << std::endl;
    } else if (e.path().filename() == strg->paths.font_mono) {
      strg->paths.font_mono = e.path().string();
      std::cout << "[FONT] Mono font: " << strg->paths.font_mono << std::endl;
    } else if (e.path().filename() == strg->paths.font_ico) {
      strg->paths.font_ico = e.path().string();
      std::cout << "[FONT] Icon font: " << strg->paths.font_ico << std::endl;
    }
  }

  static const float fonts_size{22.0f};
  static const float edits_size{fonts_size - 0.0f};
  static const float icons_size{fonts_size * 4.0f / 5.0f};
  static const ImWchar icons_ranges[] = {ICON_MIN_FA, ICON_MAX_16_FA, 0};
  ImFontConfig icons_config;
  icons_config.MergeMode = true;
  icons_config.PixelSnapH = true;
  icons_config.GlyphMinAdvanceX = icons_size;

  if (access(strg->paths.font_default.c_str(), F_OK) == 0) {
    strg->fonts.normal = io.Fonts->AddFontFromFileTTF(strg->paths.font_default.c_str(), fonts_size);
    if (access(strg->paths.font_ico.c_str(), F_OK) == 0)
      io.Fonts->AddFontFromFileTTF(strg->paths.font_ico.c_str(), icons_size, &icons_config,
                                   icons_ranges);
  }

  if (access(strg->paths.font_italic.c_str(), F_OK) == 0)
    strg->fonts.italic = io.Fonts->AddFontFromFileTTF(strg->paths.font_italic.c_str(), edits_size);

  if (access(strg->paths.font_mono.c_str(), F_OK) == 0)
    strg->fonts.mono = io.Fonts->AddFontFromFileTTF(strg->paths.font_mono.c_str(), fonts_size + 2);
}
