#pragma once

#include <unistd.h>
#include "../modules/ini/SimpleIni.h"
#include "utils/storage.hpp"

class Load {
private:
  CSimpleIniA si_conf, si_imgui; // Simple Ini parsers
  Storage *strg;                 // Pointer to my storage.h
  void imgui_font_loader(ImGuiIO &);

public:
  Load(Storage *strg) { this->strg = strg; };
  void init();
  void imgui_config(ImGuiIO &);
  void cleanup();
};
