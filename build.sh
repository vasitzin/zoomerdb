#!/bin/bash

# [ -f /usr/bin/clang ] && export CC=/usr/bin/clang
# [ -f /usr/bin/clang++ ] && export CXX=/usr/bin/clang++

cmake_empty=false
cmake_target="linux"
cmake_type="debug"
cmake_pkg="none"
cmake_gapi="both"
cores=$(($(grep -m 1 'cpu cores' < /proc/cpuinfo | cut -d: -f2 | cut -c2-)-1))
[ $cores -lt 1 ] && cores=1

while getopts 'ep:t:k:g:h' opt; do
  case "$opt" in
    e) cmake_empty=true;;
    p)
      case "$OPTARG" in
        linux | mingw) cmake_target="$OPTARG";;
        *) exit 99;;
      esac
      ;;
    t)
      case "$OPTARG" in
        debug | release) cmake_type="$OPTARG";;
        *) exit 89;;
      esac
      ;;
    k)
      case "$OPTARG" in
        none | appimg) cmake_pkg="$OPTARG";;
        *) exit 79;;
      esac
      ;;
    g)
      case "$OPTARG" in
        both | vulkan | opengl) cmake_gapi="$OPTARG";;
        *) exit 69;;
      esac
      ;;
    h)
      echo "./build.sh <-e> <-p linux|mingw> <-t debug|release> <-k none|appimg> <-g both|vulkan|opengl>"
      echo "-e erase the build directory contents."
      echo "-p <linux|mingw> cross-compile to specified target platform. Default: linux"
      echo "-t <debug|release> the build you want to set for cmake. Default: debug"
      echo "-k <none|appimg> specify packaging method, only for linux. Default: none"
      echo "-g <both|vulkan|opengl> choose api to compile with. Default: both"
      exit 0
      ;;
  esac
done; shift "$(($OPTIND - 1))"

[ ! -d "build/$cmake_type" ] && mkdir -p "build/$cmake_type"

cd "build/$cmake_type" || return 1

[ "$cmake_empty" = true ] && rm -rf ./*

if [ "${cmake_target}" == "linux" ]; then
  cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE="${cmake_type^}" -DGAPI="${cmake_gapi}" -DPKG="${cmake_pkg}" ../..
  cmake --build . -- -j $cores
elif [ "${cmake_target}" == "mingw" ]; then
  mingw64-cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE="${cmake_type^}" -DGAPI="${cmake_gapi}" -DCMAKE_TOOLCHAIN_FILE="tc-mingw.cmake" ../..
  mingw64-make -j $cores
fi

cd ../..
