#include "loop.hpp"
#include "../modules/IconsFontAwesome6.h"
#include "../modules/zoomergui/zoomergui.hpp"

void Loop::init() {
  // Load Tables
  // TODO utilize multiple threads
  for (fs::directory_entry entry : fs::directory_iterator(strg->paths.data_parent)) {
    std::string file_path{entry.path().string()};
    switch (strg->options.launch_panels) {
    case all:
      if (ZDB::is_zdb(entry)) {
        ctrl.load_zdb(file_path, &loaded);
        TableView panel{TableView(&ctrl, strg, loaded.at(loaded.size() - 1).table.get_uuid())};
        tableviews.insert({loaded.at(loaded.size() - 1).table.get_uuid(), panel});
      }
      break;
    case session:
      if (ZDB::is_zdb(entry)) {
        zdb_uuid_t uuid;
        {
          std::ifstream file{file_path};
          ZDB::read_uuid(file, &uuid);
        }
        bool found{false};
        for (size_t i{0}; i < strg->options.open_panels.size(); ++i) {
          if (uuid == strg->options.open_panels[i]) {
            ctrl.load_zdb(file_path, &loaded);
            TableView panel{TableView(&ctrl, strg, loaded.at(loaded.size() - 1).table.get_uuid())};
            tableviews.insert({loaded.at(loaded.size() - 1).table.get_uuid(), panel});
            loaded.at(loaded.size() - 1).visible = strg->options.state_panels[i];
            found = true;
            break;
          }
        }
        if (!found) ctrl.store_zdb(file_path, &stored);
      }
      break;
    case none:
    default:
      if (ZDB::is_zdb(entry)) ctrl.store_zdb(file_path, &stored);
      break;
    }
  }
  strg->options.open_panels = {};
  strg->options.state_panels = {};
}

/**
 * - NEVER DO IN HERE:
 *   1. create variables if avoidable
 *   2. use the 'for-each' loop, eg: for (auto el : vec) {...}
 * - INSTEAD TRY TO:
 *   1. use pointers
 *   2. use the trad 'for' loop, eg: for (int i{0}; i < 10; ++i) {...}
 */
void Loop::render() {
  // ## HOTKEYS ##
  if (ImGui::IsKeyDown(ImGuiKey_ModCtrl) && ImGui::IsKeyPressed(ImGuiKey_Q))
    htks.terminate_process();
  if (ImGui::IsKeyDown(ImGuiKey_ModCtrl) && ImGui::IsKeyPressed(ImGuiKey_N))
    strg->panels.create_table = true;
#ifdef METRICS
  if (ImGui::IsKeyDown(ImGuiKey_ModCtrl) && ImGui::IsKeyPressed(ImGuiKey_B)) htks.toggle_metrics();
#endif

  // ## MENUBAR ##
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::BeginMenu("File")) { // # FILE MENU
      {
        if (ImGui::MenuItem(ICON_FA_FOLDER_PLUS " New Table", "CTRL+N"))
          strg->panels.create_table = true;
        ImGui::SameLine();
        ZoomGui::HelpMarker("The collection of entries stored in a table format.");
      }
      {
        if (ImGui::BeginMenu(ICON_FA_HARD_DRIVE " Load Table")) {
          if (stored.size() > 0) {
            for (size_t i{0}; i < stored.size(); ++i) {
              ImGui::PushID(i);
              if (ImGui::Selectable(stored.at(i).title.c_str())) ctrl.load_stored_container(i);
              ImGui::PopID();
            }
          } else {
            ImGui::TextDisabled("nothing to load...");
          }
          ImGui::EndMenu();
        }
      }
      if (ImGui::MenuItem(ICON_FA_RIGHT_FROM_BRACKET " Exit", "CTRL+Q")) strg->quit = true;
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }

  if (strg->panels.create_table) createview.view(&loaded, &strg->panels.create_table);
  if (strg->panels.info) aboutview.view();

  for (size_t i{0}; i < loaded.size(); ++i) {
    tableviews[loaded.at(i).table.get_uuid()].menu(loaded.at(i));
    if (loaded.at(i).visible) tableviews[loaded.at(i).table.get_uuid()].view(&loaded.at(i));
  }

  settingsview.menu();
  aboutview.menu();

  // ## ENDLOOP ##
  // Operations modifying loaded and stored table vectors
  // are executed at the end of the loop to let the frame
  // be drawn without issues.
  switch (ctrl.get_operation()) {
  case do_load: {
    ctrl.load_stored(&stored, &loaded);
    TableView panel{TableView(&ctrl, strg, loaded.at(loaded.size() - 1).table.get_uuid())};
    tableviews.insert({loaded.at(loaded.size() - 1).table.get_uuid(), panel});
    break;
  }
  case do_reload: {
    ctrl.reload(&loaded);
    break;
  }
  case do_unload: {
    zdb_uuid_t removed_uuid{ctrl.unload(&stored, &loaded)};
    tableviews.erase(removed_uuid);
    break;
  }
  case do_duplicate: {
    ctrl.duplicate(&loaded);
    TableView panel{TableView(&ctrl, strg, loaded.at(loaded.size() - 1).table.get_uuid())};
    tableviews.insert({loaded.at(loaded.size() - 1).table.get_uuid(), panel});
    break;
  }
  case do_add_table: {
    ctrl.add_table(&loaded);
    TableView panel{TableView(&ctrl, strg, loaded.at(loaded.size() - 1).table.get_uuid())};
    tableviews.insert({loaded.at(loaded.size() - 1).table.get_uuid(), panel});
    break;
  }
  case do_rename_table: {
    ctrl.rename_table(&loaded);
    break;
  }
  case do_remove_table: {
    ctrl.remove_table(&loaded);
    break;
  }
  case do_add_header: {
    ctrl.add_header(&loaded);
    break;
  }
  case do_set_header: {
    ctrl.set_header(&loaded);
    break;
  }
  case do_remove_header: {
    ctrl.remove_header(&loaded);
    break;
  }
  default:
    break;
  }
}

void Loop::store_session() {
  for (LoadedZDB l : loaded) {
    strg->options.open_panels.push_back(l.table.get_uuid());
    strg->options.state_panels.push_back(l.visible);
  }
}
