FROM fedora:40

ARG DOCKERER=user
ARG GLFWVER=3.4

RUN useradd -m $DOCKERER && usermod -aG wheel $DOCKERER
RUN echo -en "\n$DOCKERER ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers

RUN dnf -y upgrade
RUN dnf -y install \
gcc \
gcc-c++ \
git \
wget \
cmake \
make \
glfw-devel \
vulkan-devel \
vulkan-validation-layers-devel \
vulkan-headers \
vulkan-loader \
vulkan-tools \
mingw64-filesystem \
mingw64-headers \
mingw64-gcc \
mingw64-gcc-c++ \
mingw64-libstdc++ \
mingw64-crt \
mingw64-binutils \
mingw64-winpthreads \
mingw64-spirv-headers \
mingw64-spirv-tools \
mingw64-vulkan-headers \
mingw64-vulkan-loader \
mingw64-vulkan-tools \
mingw64-vulkan-validation-layers \
mingw64-vulkan-utility-libraries \
mingw-w64-tools \
;

WORKDIR /opt
RUN wget "https://github.com/glfw/glfw/releases/download/$GLFWVER/glfw-$GLFWVER.zip"
RUN unzip -q "/opt/glfw-$GLFWVER.zip"
RUN mingw64-cmake -S/opt/glfw-$GLFWVER -B/opt/build-glfw-$GLFWVER -DCMAKE_TOOLCHAIN_FILE=/opt/glfw-$GLFWVER/CMake/x86_64-w64-mingw32.cmake -DGLFW_BUILD_DOCS=OFF -DBUILD_SHARED_LIBS=OFF

WORKDIR /opt/build-glfw-$GLFWVER
RUN mingw64-make install

WORKDIR /
RUN rm -rf /opt/glfw-$GLFWVER.zip /opt/glfw-$GLFWVER

USER $DOCKERER
