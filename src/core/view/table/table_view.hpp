#pragma once

#include "../../utils/storage.hpp"
#include "../create/create_view.hpp"

class TableView {
private:
  struct Coords {
    size_t row{0};
    size_t col{0};
  };

  const ImGuiTableFlags win_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar;
  const ImGuiTableFlags header_flags = ImGuiTableColumnFlags_WidthStretch;
  const ImGuiTableFlags popup_flags = ImGuiWindowFlags_NoMove;
  const ImGuiTableFlags table_flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_SizingFixedFit |
                                      ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable |
                                      ImGuiTableFlags_Reorderable | ImGuiTableFlags_Hideable;
  Storage *strg{nullptr};
  Controller *ctrl{nullptr};
  CreateViewEntry createview{CreateViewEntry()};
  zdb_uuid_t table_uuid;
  bool editing_head{false}, editing_cell{false}, entry_panel{false};
  Coords head_coords{}, cell_coords{};
  char new_title[N_TITLE]{};
  char add_new_header[N_HEADER]{};
  char set_new_header[N_HEADER]{};
  char set_new_value[N_VALUE]{};

  inline void cell_popup_contents(size_t, size_t, LoadedZDB &);

public:
  TableView() {};
  TableView(Controller *ctrl, Storage *strg) {
    this->ctrl = ctrl;
    this->strg = strg;
    this->createview.set_controller(ctrl);
  };
  TableView(Controller *ctrl, Storage *strg, zdb_uuid_t table_uuid) {
    this->table_uuid = table_uuid;
    this->ctrl = ctrl;
    this->strg = strg;
    this->createview.set_controller(ctrl);
  };
  void menu(LoadedZDB &);
  void view(LoadedZDB *);
};
