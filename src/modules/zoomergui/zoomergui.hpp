#pragma once

#include <string>
#include "../imgui/imgui_internal.h"
#include "../imgui/imgui.h"
#include "../zdb/zdb.hpp"

const static ImGuiSeparatorFlags vsep_flags = ImGuiSeparatorFlags_Vertical;
const static ImVec4 ZoomerCol_Empty = ImVec4{.0f, .0f, .0f, .0f};

namespace ZoomGui {
void VSeperator();
void HelpMarker(const char *);
void WarningMarker(const char *);
void SelectableText(const char *);
void EditableText(const char *, const char *, char *, size_t);
std::string get_rich_title(ZDB &);
std::string get_win_title(ZDB &);
}; // namespace ZoomGui
