#pragma once

#include <unistd.h>
#include <map>
#include "utils/controller.hpp"
#include "utils/storage.hpp"
#include "utils/hotkeys.hpp"
#include "view/settings/settings_view.hpp"
#include "view/table/table_view.hpp"
#include "view/create/create_view.hpp"
#include "view/about/about_view.hpp"

/**
 * TODO impl this as a setting!
Limit the fps

double prev_glfwtime{glfwGetTime()}; // before frame loop
...
if (frame_limit != -1) { // this should be here
 while (glfwGetTime() < prev_glfwtime + 1 / frame_limit) {...}
 prev_glfwtime += 1 / frame_limit;
}
*/

class Loop {
private:
  Storage *strg;                              // Pointer to (Global) Storage
  HotKeys htks;                               // Pointer to HotKeys
  Controller ctrl;                            // Controller
  std::vector<StoredZDB> stored{};            // zdb files and the titles they contain
  std::vector<LoadedZDB> loaded{};            // Tables and their corresponding zdb files
  std::map<zdb_uuid_t, TableView> tableviews; // Table Panels Map
  CreateViewTable createview;                 // Create Table Panel
  SettingsView settingsview;                  // Settings Menu
  AboutView aboutview;                        // About Panel

public:
  Loop(Storage *strg) {
    this->strg = strg;
    this->htks = HotKeys(strg);    // Pointer to HotKeys
    this->ctrl = Controller(strg); // Controller
    this->createview.set_controller(&this->ctrl);
    this->settingsview.set_strg(strg);
    this->aboutview.set_strg(strg);
  };
  void init();
  void render();
  void store_session();
};
