#!/bin/bash

IMG="zoomerdb"
PRJ="$(pwd)"
DIR="$HOME"

su="sudo"
[ -f /usr/bin/doas ] && su="doas"

images=($(docker image ls | grep "$IMG" | wc -l))
if [ $images -lt 1 ]; then
  docker build --build-arg DOCKERER=$USER -t $IMG . || exit 99
fi

$su docker run -it -v ~/.bash_history:$DIR/.bash_history -v .:$PRJ -w $PRJ --rm $IMG $@
