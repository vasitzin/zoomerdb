#include "main_gl.hpp"
#include "core/load.hpp"
#include "core/loop.hpp"
#include "modules/imgui/backends/imgui_impl_glfw.h"
#include <cstdlib>

#ifndef TITLE
#define TITLE "Untitled"
#endif

int main(int, char **) {
#ifdef __linux__
  std::string session = getenv("XDG_SESSION_TYPE");
  transform(session.begin(), session.end(), session.begin(), ::tolower);
#endif
  Storage strg = Storage();
  Load load = Load(&strg);
  Loop loop = Loop(&strg);
  load.init();

  // Setup GLFW window
  const char *glsl_version = "#version 130";
  glfwSetErrorCallback(glfw_error_callback);
  if (!glfwInit()) return 1;
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_DECORATED, true);
  GLFWwindow *window =
      glfwCreateWindow(strg.options.win_width, strg.options.win_height, TITLE, NULL, NULL);
  glfwMakeContextCurrent(window);
  glfwSetWindowPos(window, strg.options.win_left, strg.options.win_top);
  glfwSetWindowIcon(window, 1, strg.glfw_images);
  // glfwSwapInterval(1); // Enable vsync

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO &io = ImGui::GetIO();
  load.imgui_config(io);

  // Setup Platform/Renderer backends
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

  loop.init();
  while (!strg.quit && !glfwWindowShouldClose(window)) {
    glfwPollEvents();

    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    strg.main_dockspace = ImGui::DockSpaceOverViewport(ImGui::GetMainViewport()->ID);
#ifdef METRICS
    if (strg.panels.metrics) ImGui::ShowMetricsWindow();
#endif

    loop.render();

    // Get window size and position
#ifdef __linux__
    if (session.compare("wayland") != 0) {
#endif
      glfwGetWindowSize(window, &strg.options.win_width, &strg.options.win_height);
      glfwGetWindowPos(window, &strg.options.win_left, &strg.options.win_top);
#ifdef __linux__
    }
#endif

    // Rendering
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w,
                 clear_color.z * clear_color.w, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    // Update and Render additional Platform Windows
    // (Platform functions may change the current OpenGL context, so we save/restore it to make it
    // easier to paste this code elsewhere.
    //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
      GLFWwindow *backup_current_context = glfwGetCurrentContext();
      ImGui::UpdatePlatformWindows();
      ImGui::RenderPlatformWindowsDefault();
      glfwMakeContextCurrent(backup_current_context);
    }

    glfwSwapBuffers(window);
  }

  // Cleanup
  loop.store_session();
  load.cleanup();
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}
