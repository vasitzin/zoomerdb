#pragma once

#include "../../../modules/imgui/imgui.h"
#include "../../utils/storage.hpp"

class AboutView {
private:
  const ImGuiTableFlags win_flags =
      ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoResize;
  const ImGuiTableFlags table_flags = ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_RowBg |
                                      ImGuiTableFlags_NoBordersInBody |
                                      ImGuiTableFlags_BordersOuter;
  Storage *strg{nullptr};

public:
  AboutView() {};
  void set_strg(Storage *strg) { this->strg = strg; };
  void menu();
  void view();
};
