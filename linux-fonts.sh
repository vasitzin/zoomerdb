#!/bin/bash

su="sudo"
package_manager=""
manual_install=true
fonts_install=true

if [ -f /usr/bin/pacman ]; then package_manager="pacman"
elif [ -f /usr/bin/dnf ]; then package_manager="dnf"
elif [ -f /usr/bin/apt ]; then package_manager="apt"
fi

while getopts 'puh' opt; do
  case "$opt" in
    p) manual_install=false;;
    u) fonts_install=false;;
    h)
      echo "./linux-fonts.sh <-p> <-u>"
      echo "-p try to use package manager instead of wget, but '-u' wont work later"
      echo "-u remove '/usr/share/fonts/zoomerdb' directory"
      exit 0
      ;;
  esac
done; shift "$(($OPTIND - 1))"
[ "$manual_install" = "" ] && exit 99
[ "$fonts_install" = "" ] && exit 89
[ -f /usr/bin/doas ] && su="doas"

mkfontdir () {
  if [ ! -d "/usr/share/fonts/zoomerdb" ]; then
    echo && echo "Creating fonts/zoomerdb directory..."
    $su mkdir -p /usr/share/fonts/zoomerdb && echo "Done."
  fi
}

font-free-install () {
  echo
  case "$(read -p "Install FreeSans? (This will fail if 'wget' is not installed on your system!) [y/N]: " choice && echo $choice)" in
    y | Y)
      mkfontdir
      $su wget -P "/usr/share/fonts/zoomerdb" --quiet --show-progress "https://raw.githubusercontent.com/opensourcedesign/fonts/master/gnu-freefont_freesans/FreeSans.otf" && echo "Done."
      ;;
  esac
}

font-jetbrains-install () {
  echo
  case "$(read -p "Install JetBrainsMono-Regular? (This will fail if 'wget' is not installed on your system!) [y/N]: " choice && echo $choice)" in
    y | Y)
      mkfontdir
      $su wget -P "/usr/share/fonts/zoomerdb" --quiet --show-progress "https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/fonts/ttf/JetBrainsMono-Regular.ttf" && echo "Done."
      ;;
  esac
}

font-awesome-install () {
  echo
  case "$(read -p "Install fa-solid-900? (This will fail if 'wget' is not installed on your system!) [y/N]: " choice && echo $choice)" in
    y | Y)
      mkfontdir
      $su wget -P "/usr/share/fonts/zoomerdb" --quiet --show-progress "https://raw.githubusercontent.com/components/font-awesome/master/webfonts/fa-solid-900.ttf" && echo "Done."
      ;;
  esac
}

if [ "$fonts_install" = true ]; then
  echo && echo "Installing fonts."
  if [ "$manual_install" = true ]; then
    font-free-install
    font-jetbrains-install
    font-awesome-install
  else
    case "$package_manager" in
      pacman) $su pacman -Syu && $su pacman -S gnu-free-fonts ttf-jetbrains-mono ttf-font-awesome;;
      apt) $su apt update && $su apt install fonts-freefont-otf fonts-jetbrains-mono fonts-font-awesome;;
      dnf) $su dnf upgrade && $su dnf install gnu-free-sans-fonts jetbrains-mono-fonts-all fontawesome-6-free-fonts;;
      *) echo "Your distro is not supported!";;
    esac
  fi
else 
  echo && echo "Removing fonts/zoomerdb dir."
  if [ "$manual_install" = true ]; then
    $su rm -rf /usr/share/fonts/zoomerdb
  fi
fi
