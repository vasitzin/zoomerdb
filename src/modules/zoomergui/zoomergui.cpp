#include "zoomergui.hpp"
#include "../IconsFontAwesome6.h"

void ZoomGui::HelpMarker(const char *desc) {
  ImGui::TextDisabled(ICON_FA_CIRCLE_QUESTION);
  if (ImGui::IsItemHovered()) {
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
    ImGui::TextUnformatted(desc);
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();
  }
}

void ZoomGui::WarningMarker(const char *desc) {
  ImGui::TextDisabled(ICON_FA_TRIANGLE_EXCLAMATION);
  if (ImGui::IsItemHovered()) {
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
    ImGui::TextUnformatted(desc);
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();
  }
}

void ZoomGui::VSeperator() {
  ImGui::SameLine();
  ImGui::SeparatorEx(vsep_flags);
  ImGui::SameLine();
}

void ZoomGui::SelectableText(const char *text) {
  ImGui::PushStyleColor(ImGuiCol_Header, ZoomerCol_Empty);
  ImGui::PushStyleColor(ImGuiCol_HeaderHovered, ZoomerCol_Empty);
  ImGui::PushStyleColor(ImGuiCol_HeaderActive, ZoomerCol_Empty);
  ImGui::Selectable(text);
  ImGui::PopStyleColor(3);
}

void ZoomGui::EditableText(const char *label, const char *hint, char *text, size_t size) {
  ImGui::PushItemWidth(-1);
  ImGui::PushStyleColor(ImGuiCol_FrameBg, ZoomerCol_Empty);
  ImGui::InputTextWithHint(label, hint, text, size);
  ImGui::SetKeyboardFocusHere(-1);
  ImGui::PopStyleColor();
  ImGui::PopItemWidth();
}

std::string ZoomGui::get_rich_title(ZDB &zdb) {
  if (zdb.get_state() != safe)
    return "*" + zdb.get_title() + "###" + zdb.get_uuid();
  else
    return zdb.get_title() + "###" + zdb.get_uuid();
}

std::string ZoomGui::get_win_title(ZDB &zdb) {
  if (zdb.get_state() != safe)
    return "*" + zdb.get_title() + "###" + zdb.get_uuid();
  else
    return zdb.get_title() + "###" + zdb.get_uuid();
}
