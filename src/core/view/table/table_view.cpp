#include "table_view.hpp"
#include "../../../modules/IconsFontAwesome6.h"
#include "../../../modules/zoomergui/zoomergui.hpp"
#include <cstddef>

void TableView::menu(LoadedZDB &wrap) {
  if (ImGui::BeginMainMenuBar()) {
    std::string menu_title{};
    if (wrap.visible)
      menu_title = ZoomGui::get_rich_title(wrap.table);
    else
      menu_title = ICON_FA_GHOST " " + ZoomGui::get_rich_title(wrap.table);
    if (ImGui::BeginMenu(menu_title.c_str())) {
      { ImGui::Checkbox(ZoomGui::get_rich_title(wrap.table).c_str(), &wrap.visible); }
      {
        if (ImGui::MenuItem(ICON_FA_FLOPPY_DISK " Save")) {
          std::ofstream file;
          if (wrap.table.get_state() == fresh) {
            wrap.dbfile =
                std::to_string(std::chrono::system_clock().now().time_since_epoch().count()) +
                strg->paths.extension;
          }
          std::string save_path{strg->paths.data_parent + strg->paths.seperator + wrap.dbfile};
          std::cout << save_path << std::endl;
          file.open(save_path, std::ios::trunc);
          wrap.table.save(file);
          file.close();
        }
        ImGui::SameLine();
        ZoomGui::WarningMarker("Overwrites old file.");
      }
      {
        if (ImGui::MenuItem(ICON_FA_ARROWS_ROTATE " Reload"))
          ctrl->reload_container(wrap.table.get_uuid());
        ImGui::SameLine();
        ZoomGui::WarningMarker("Any unsaved changes will be lost.");
      }
      {
        if (ImGui::MenuItem(ICON_FA_ARROW_UP_FROM_BRACKET " Unload"))
          ctrl->unload_container(wrap.table.get_uuid());
        ImGui::SameLine();
        ZoomGui::WarningMarker("Any unsaved changes will be lost.");
      }
      {
        if (ImGui::MenuItem(ICON_FA_TRASH_CAN " Delete")) {
          std::cout << "in delete button" << std::endl;
          ctrl->remove_table_container(wrap.table.get_uuid());
          std::cout << "after button operation: " << ctrl->get_operation() << std::endl;
        }
        ImGui::SameLine();
        ZoomGui::WarningMarker("Table is deleted from your device.");
      }
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void TableView::view(LoadedZDB *wrap) {
  ImGui::SetNextWindowDockID(strg->main_dockspace, ImGuiCond_FirstUseEver);
  ImGui::Begin(ZoomGui::get_win_title(wrap->table).c_str(), &wrap->visible, win_flags);
  if (ImGui::BeginMenuBar()) {
    if (!this->entry_panel) {
      if (ImGui::BeginMenu(ICON_FA_PENCIL " Rename")) {
        if (ImGui::IsItemHovered()) strcpy(this->new_title, wrap->table.get_title().c_str());
        ImGui::InputText("##NEW_TITLE", this->new_title, N_TITLE);
        if (ImGui::IsKeyPressed(ImGuiKey_Enter)) {
          zdb_title_t new_title{this->new_title};
          ctrl->rename_table_container(wrap->table.get_uuid(), new_title);
          ImGui::CloseCurrentPopup();
        }
        ImGui::EndMenu();
      }
      if (ImGui::BeginMenu(ICON_FA_FILE_PEN " Edit Headers")) {
        if (ImGui::IsItemHovered()) strcpy(this->add_new_header, "");
        ImGui::SeparatorText("Add new Header");
        ImGui::InputText("##ADD_NEW_HEADER", this->add_new_header, N_HEADER);
        ImGui::SameLine();
        if (ImGui::Button(ICON_FA_PLUS)) {
          zdb_header_t new_header{this->add_new_header};
          ctrl->add_header_container(wrap->table.get_uuid(), new_header);
        }
        ImGui::SameLine();
        ZoomGui::HelpMarker("An empty value will be appended to all your Table entries.");
        ImGui::SeparatorText("Edit Existing");
        for (size_t j{0}; j < wrap->table.get_headers().size(); ++j) {
          ImGui::PushID(j);
          if (ImGui::Button(ICON_FA_TRASH_CAN))
            ctrl->remove_header_container(wrap->table.get_uuid(), j);
          ImGui::SameLine();
          if (this->editing_head && this->head_coords.col == j) {
            ImGui::PushFont(strg->fonts.italic);
            ZoomGui::EditableText("##SET_NEW_HEADER", "empty", this->set_new_header, N_HEADER);
            ImGui::PopFont();
            if (ImGui::IsKeyDown(ImGuiKey_Enter) || ImGui::IsKeyDown(ImGuiKey_Tab)) {
              zdb_header_t new_header{this->set_new_header};
              ctrl->set_header_container(wrap->table.get_uuid(), j, new_header);
              this->editing_head = false;
            } else if (ImGui::IsKeyDown(ImGuiKey_Escape)) {
              this->editing_head = false;
            }
          } else {
            ZoomGui::SelectableText(wrap->table.get_headers().at(j).c_str());
            if (ImGui::IsItemHovered()) {
              if (ImGui::IsMouseClicked(0) || ImGui::IsMouseClicked(1)) {
                this->editing_head = true;
                this->head_coords = {0, j};
                strcpy(this->set_new_header, wrap->table.get_headers().at(j).c_str());
              }
            }
          }
          ImGui::PopID();
        }
        ImGui::EndMenu();
      } else {
        this->editing_head = false;
      }
      if (ImGui::MenuItem(ICON_FA_CLONE " Duplicate"))
        ctrl->duplicate_container(wrap->table.get_uuid());
      if (!strg->panels.create_table) {
        if (ImGui::MenuItem(ICON_FA_FILE_CIRCLE_PLUS " Add Entry")) this->entry_panel = true;
      } else {
        ImGui::TextDisabled(ICON_FA_FILE_CIRCLE_PLUS " Add Entry");
      }
    }
    ImGui::EndMenuBar();
  }
  if (ImGui::BeginTable("##TABLE", wrap->table.get_headers().size(), table_flags)) {
    {
      for (size_t i{0}; i < wrap->table.get_headers().size(); ++i)
        ImGui::TableSetupColumn(wrap->table.get_headers().at(i).c_str(), header_flags);
      ImGui::TableHeadersRow();
    }
    {
      for (size_t row{0}; row < wrap->table.get_entries().size(); ++row) {
        ImGui::TableNextRow(ImGuiTableRowFlags_None, strg->fonts.normal->FontSize + 10.f);
        ImGui::PushID(row);
        for (size_t col{0}; col < wrap->table.get_headers().size(); ++col) {
          ImGui::TableNextColumn();
          ImGui::AlignTextToFramePadding();
          ImGui::PushID(col);
          if (this->editing_cell && this->table_uuid == wrap->table.get_uuid() &&
              this->cell_coords.row == row && this->cell_coords.col == col) {
            ImGui::Text(ICON_FA_PEN);
            ImGui::SameLine();
            ImGui::PushFont(strg->fonts.italic);
            ZoomGui::EditableText("##SET_NEW_VALUE", "empty", this->set_new_value, N_VALUE);
            ImGui::PopFont();
            if (ImGui::IsKeyDown(ImGuiKey_Enter)) {
              ctrl->set_value_container(row, col, this->set_new_value);
              this->editing_cell = false;
            } else if (ImGui::IsKeyDown(ImGuiKey_Escape)) {
              this->editing_cell = false;
            }
          } else {
            ZoomGui::SelectableText(wrap->table.get_entries().at(row).at(col).c_str());
            if (ImGui::IsItemHovered()) {
              if (ImGui::IsMouseClicked(1)) {
                this->cell_coords = {row, col};
                strcpy(this->set_new_value, wrap->table.get_entries().at(row).at(col).c_str());
                ImGui::OpenPopup("##ENTRY_POPUP");
              } else if (ImGui::IsMouseDoubleClicked(0)) {
                this->editing_cell = true;
                this->cell_coords = {row, col};
                strcpy(this->set_new_value, wrap->table.get_entries().at(row).at(col).c_str());
              }
            }
            cell_popup_contents(this->cell_coords.row, this->cell_coords.col, *wrap);
          }
          ImGui::PopID();
        }
        ImGui::PopID();
      }
    }
    ImGui::EndTable();
  }
  ImGui::End();

  if (entry_panel) createview.view(wrap, &entry_panel);

  // ## ENDTABLE ##
  // Operations modifying table entries are executed
  // at the end of the loop to let the tableview
  // be drawn inside the frame without issues.
  switch (ctrl->get_operation()) {
  case do_add_entry: {
    ctrl->add_entry(wrap);
    break;
  }
  case do_set_value: {
    ctrl->set_value(wrap);
    break;
  }
  case do_move_entry: {
    ctrl->move_entry(wrap);
    break;
  }
  case do_remove_entry: {
    ctrl->remove_entry(wrap);
    break;
  }
  default:
    break;
  }
}

inline void TableView::cell_popup_contents(size_t row, size_t col, LoadedZDB &wrap) {
  if (ImGui::BeginPopup("##ENTRY_POPUP", popup_flags)) {
    if (ImGui::Selectable(ICON_FA_PENCIL " Edit##EDIT_ENTRY")) {
      this->editing_cell = true;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Selectable(ICON_FA_CLIPBOARD " Copy##CLIPBOARD")) {
      ImGui::SetClipboardText(wrap.table.get_entries().at(row).at(col).c_str());
      ImGui::CloseCurrentPopup();
    }
    ImGui::Separator();
    if (ImGui::Selectable(ICON_FA_ANGLES_UP " Top##MOVE_ENTRY_TOP")) {
      ctrl->move_entry_container(0, row);
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Selectable(ICON_FA_ANGLE_UP " Up##MOVE_ENTRY_UP")) {
      ctrl->move_entry_container(row - 1, row);
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Selectable(ICON_FA_ANGLE_DOWN " Down##MOVE_ENTRY_DOWN")) {
      ctrl->move_entry_container(row + 1, row);
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Selectable(ICON_FA_ANGLES_DOWN " Bottom##MOVE_ENTRY_BOT")) {
      ctrl->move_entry_container(wrap.table.get_entries().size() - 1, row);
      ImGui::CloseCurrentPopup();
    }
    ImGui::Separator();
    if (ImGui::Selectable(ICON_FA_TRASH_CAN " Delete##DELETE_ENTRY")) {
      ctrl->remove_entry_container(row);
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
}
