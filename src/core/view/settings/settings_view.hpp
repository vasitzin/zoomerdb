#pragma once

#include "../../utils/storage.hpp"

class SettingsView {
private:
  Storage *strg{nullptr};

public:
  SettingsView(){};
  void set_strg(Storage *strg) { this->strg = strg; };
  void menu();
};
