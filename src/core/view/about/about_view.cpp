#include "about_view.hpp"

void AboutView::menu() {
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::BeginMenu("About")) {
      ImGui::Text("%s", strg->flavor_str.c_str());
      ImGui::Text("%s", strg->version_str.c_str());
      if (ImGui::MenuItem("More Info##ZOOMERDB_INFO")) strg->panels.info = !strg->panels.info;
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}

void AboutView::view() {
  ImGui::Begin("App Info", &strg->panels.info, win_flags);
  ImGui::PushFont(strg->fonts.mono);
  ImGui::TextColored(
      ImVec4(.45f, .130f, .37f, 1.0f),
      "                                                               888 888              ");
  ImGui::TextColored(
      ImVec4(.39f, .147f, .44f, 1.0f),
      "                                                               888 888              ");
  ImGui::TextColored(
      ImVec4(.33f, .162f, .5f, 1.0f),
      "                                                               888 888              ");
  ImGui::TextColored(
      ImVec4(.26f, .182f, .57f, 1.0f),
      "88888888  .d88b.   .d88b.  88888b.d88b.   .d88b.  888d888  .d88888 88888b.          ");
  ImGui::TextColored(
      ImVec4(.17f, .207f, .67f, 1.0f),
      "   d88P  d88\"\"88b d88\"\"88b 888 \"888 \"88b d8P  Y8b 888P\"   d88\" 888 888 \"88b");
  ImGui::TextColored(
      ImVec4(.12f, .22f, .72f, 1.0f),
      "  d88P   888  888 888  888 888  888  888 88888888 888     888  888 888  888         ");
  ImGui::TextColored(
      ImVec4(.07f, .236f, .78f, 1.0f),
      " d88P    Y88..88P Y88..88P 888  888  888 Y8b.     888     Y88b 888 888 d88P         ");
  ImGui::TextColored(
      ImVec4(0.0f, .255f, .85f, 1.0f),
      "88888888  \"Y88P\"   \"Y88P\"  888  888  888  \"Y8888  888      \"Y88888 88888P\"   ");
  ImGui::NewLine();
  ImGui::Text("%s", strg->version_str.c_str());
  ImGui::NewLine();
  ImGui::NewLine();
  ImGui::Text("by Vasileios Tzinis <vasileios.tzinis@tutanota.com>");
  ImGui::NewLine();

  ImGui::SeparatorText("fonts");
  if (ImGui::BeginTable("##FONTS_TABLE", 3, table_flags)) {
    ImGui::TableNextColumn();
    ImGui::Text("FreeSans");
    ImGui::TableNextColumn();
    ImGui::Text("www.gnu.org/software/freefont");
    ImGui::TableNextColumn();
    ImGui::Text("GPLv3+");
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text("JetBrains Mono");
    ImGui::TableNextColumn();
    ImGui::Text("www.jetbrains.com/lp/mono");
    ImGui::TableNextColumn();
    ImGui::Text("SIL-OFLv1.1");
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text("FontAwesome");
    ImGui::TableNextColumn();
    ImGui::Text("github.com/FortAwesome/Font-Awesome");
    ImGui::TableNextColumn();
    ImGui::Text("CC-BYv4.0 & SIL-OFLv1.1");
    ImGui::EndTable();
  }
  ImGui::NewLine();
  ImGui::SeparatorText("libraries");
  if (ImGui::BeginTable("##LIBS_TABLE", 3, table_flags)) {
    ImGui::TableNextColumn();
    ImGui::Text("GLFW3");
    ImGui::TableNextColumn();
    ImGui::Text("www.glfw.org");
    ImGui::TableNextColumn();
    ImGui::Text("The zlib/libpng License");
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text("Dear ImGui");
    ImGui::TableNextColumn();
    ImGui::Text("github.com/ocornut/imgui");
    ImGui::TableNextColumn();
    ImGui::Text("MIT");
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text("SimpleIni");
    ImGui::TableNextColumn();
    ImGui::Text("github.com/brofield/simpleini");
    ImGui::TableNextColumn();
    ImGui::Text("MIT");
    ImGui::EndTable();
  }
  ImGui::PopFont();
  ImGui::End();
}
