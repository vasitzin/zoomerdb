# ZoomerDB

| [What is this](#what-is-this) :: [What it is not](#what-it-is-not) :: [Roadmap](#roadmap) :: [Releases](#releases) :: [Compile](#compile) |
| :---------------------------------------------------------------------------------------------------------------------------------------- |

![zoomerdb](/uploads/9afe12efed78c9a48ee2e6c7cca45692/zoomerdb.png)

## What is this

ZoomerDB is a general purpose crossplatform **desktop database** application in which users can store data they need to keep track of, like tv shows and daily expenses.

## What it is not

This is **not a secure database**. Apart from the fact that this application doesn't connect to the web, no safety features (like encryption) have been implemented to guarantee the safety of your data, so please refrain from storing any sensitive information like passwords while using this application.

## Roadmap

- [ ] [DB] Parent/Child Relations between Tables
- [ ] [UI] CLI support for some features
- [ ] [UI] add a basic way to sort entries
- [ ] [UI] toggle between dark and light mode
- [ ] [UI] implement graphs
- [ ] [UI] implement notifications
- [ ] [TEST] implement a ctest system

## Releases

See the [Releases](https://gitlab.com/vasitzin/zoomerdb/-/releases) page for pre-compiled binaries.

For Linux users the following are optional:

Copy the icon `zoomerdb.png` from the root project directory to the `/usr/share/icons/hicolor/48x48/apps` system directory.

Also you will need to install the following fonts

- **FreeSans** (FreeSans.otf)
- **JetBrainsMono** (JetBrainsMono-Regular.ttf)
- **FontAwesome6** (fa-solid-900.ttf)

by either manually downloading and placing the files yourself, by using your package manager or by using the `install-fonts.sh` script.

## Compile (Linux)

#### Prerequisites

Clone this repository with:

```bash
git clone --recurse-submodules https://gitlab.com/vasitzin/zoomerdb.git
```

Use the provided `./docker.sh` script which installs all necessary tools inside a docker container, alternatively ignore the script and use `docker` directly if you want.

Optionally install the fonts:

- **FreeSans** (FreeSans.otf)
- **JetBrainsMono** (JetBrainsMono-Regular.ttf)
- **FontAwesome6** (fa-solid-900.ttf)

if not installed a default font will be used.

#### Build

Use the `build.sh` script at the root of the project directory. It will use your physical cores minus one.

```
❱ ./build.sh -h
./build.sh <-e> <-m> <-p linux|mingw> <-t debug|release> <-g both|vulkan|opengl>
-e clean the build directory
-m compile all sources into one big binary
-p <linux|mingw> cross-compile to specified target platform. Default: linux
-t <debug|release> the build you want to set for cmake. Default: debug
-g <both|vulkan|opengl> choose api to compile with. Default: both
```

#### Install

Use the `install.sh` script at the root of the project directory. Like `build.sh` you can use
the `-t` option and optionally
the `-u` to **un**install.

The `install-fonts.sh` script trys to install the necessary fonts and it can take
the `-m` option which downloads (by using `wget`) the font files inside `/usr/share/fonts` and optionally
the `-u` to **un**install.
