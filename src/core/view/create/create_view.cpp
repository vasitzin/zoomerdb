#include "create_view.hpp"
#include "../../../modules/IconsFontAwesome6.h"

inline zdb_headers_t CreateView::cstruct_to_string(std::vector<cheader> &new_vec_c) {
  zdb_headers_t new_vec;
  for (size_t i{0}; i < new_vec_c.size(); ++i) {
    if ((std::string)new_vec_c.at(i).header == "")
      throw "Error empty Header field!";
    else
      new_vec.push_back((std::string)new_vec_c.at(i).header);
  }
  return new_vec;
}

inline zdb_entry_t CreateView::cstruct_to_string(std::vector<cvalue> &new_vec_c) {
  zdb_entry_t new_vec;
  for (size_t i{0}; i < new_vec_c.size(); ++i)
    new_vec.push_back((std::string)new_vec_c.at(i).value);
  return new_vec;
}

void CreateViewTable::view(std::vector<LoadedZDB> *wraps, bool *view_panel) {
  ImGui::SetNextWindowSize(win_size);
  ImGui::Begin("Create new Table", nullptr, win_flags);
  {
    ImGui::Text("Table Title");
    ImGui::PushItemWidth(-1);
    ImGui::InputTextWithHint("##INPUT_TITLE", "a title is required", new_title_c, N_TITLE);
    ImGui::PopItemWidth();
  }
  ImGui::NewLine();
  {
    ImGui::Text("Headers");
    ImGui::SameLine();
    if (ImGui::Button(ICON_FA_PLUS " Add Header")) {
      cheader chead{};
      new_headers_c.push_back(chead);
    }
    bool delete_bool{false};
    size_t delete_index{0};
    for (size_t i{0}; i < new_headers_c.size(); ++i) {
      ImGui::PushID(i);
      if (ImGui::Button(ICON_FA_X "##DELETE_HEADER") && new_headers_c.size() > 1) {
        delete_bool = true;
        delete_index = i;
      }
      ImGui::SameLine();
      ImGui::PushItemWidth(-1);
      ImGui::InputTextWithHint("##INPUT_HEADER", "header name is required",
                               new_headers_c.at(i).header, N_HEADER);
      ImGui::PopItemWidth();
      ImGui::PopID();
    }
    if (delete_bool) new_headers_c.erase(new_headers_c.begin() + delete_index);
  }
  ImGui::NewLine();
  if (ImGui::Button(ICON_FA_FACE_SMILE " Create Table")) {
    try {
      new_title = new_title_c;
      if (new_title == "") throw "Error empty Title field!";
      new_headers = cstruct_to_string(new_headers_c);
      ZDB new_table = ZDB(new_title, new_headers);
      ctrl->add_table_container(new_table);
    } catch (const char *err) { std::cout << err << std::endl; }
    *view_panel = false;
    clear();
  }
  ImGui::SameLine();
  if (ImGui::Button(ICON_FA_FACE_FROWN " Cancel")) {
    *view_panel = false;
    clear();
  }
  ImGui::End();
}

void CreateViewEntry::view(LoadedZDB *wrap, bool *view_panel) {
  ImGui::SetNextWindowSize(win_size);
  ImGui::Begin((win_title + "##" + wrap->table.get_uuid()).c_str(), nullptr, win_flags);
  if (new_entry_c.empty())
    for (size_t i{0}; i < wrap->table.get_headers().size(); ++i) new_entry_c.push_back({});
  for (size_t i{0}; i < wrap->table.get_headers().size(); ++i) {
    ImGui::PushID(i);
    ImGui::Text("%s", wrap->table.get_headers().at(i).c_str());
    ImGui::PushItemWidth(-1);
    ImGui::InputTextWithHint("##INPUT_ENTRY", "empty", new_entry_c.at(i).value, N_VALUE);
    ImGui::PopItemWidth();
    ImGui::PopID();
  }
  ImGui::NewLine();
  if (ImGui::Button(ICON_FA_FILE_CIRCLE_CHECK " Add")) {
    try {
      new_entry = cstruct_to_string(new_entry_c);
      ctrl->add_entry_container(new_entry);
      *view_panel = false;
      clear();
    } catch (const char *err) { std::cout << err << std::endl; }
  }
  ImGui::SameLine();
  if (ImGui::Button(ICON_FA_FILE_CIRCLE_XMARK " Cancel")) {
    *view_panel = false;
    clear();
  }
  ImGui::End();
}
