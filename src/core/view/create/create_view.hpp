#pragma once

#include <string>
#include <vector>
#include "../../utils/controller.hpp"

class CreateView {
protected:
  struct cheader {
    char header[N_HEADER]{};
  };
  struct cvalue {
    char value[N_VALUE]{};
  };
  const ImGuiTableFlags win_flags =
      ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoDocking;
  const ImVec2 win_size{360, 512};

  inline zdb_headers_t cstruct_to_string(std::vector<cheader> &);
  inline zdb_entry_t cstruct_to_string(std::vector<cvalue> &);
};

class CreateViewTable : CreateView {
private:
  Controller *ctrl{nullptr};
  zdb_title_t new_title;
  zdb_headers_t new_headers;
  char new_title_c[N_TITLE];
  std::vector<cheader> new_headers_c;

public:
  CreateViewTable() {
    this->new_title = {};
    this->new_headers = {};
    strcpy(this->new_title_c, "");
    this->new_headers_c = {};
    this->new_headers_c.push_back({});
  };

  void set_controller(Controller *ctrl) { this->ctrl = ctrl; };
  void view(std::vector<LoadedZDB> *, bool *);
  void clear() {
    this->new_title = {};
    this->new_headers = {};
    strcpy(this->new_title_c, "");
    this->new_headers_c = {};
    this->new_headers_c.push_back({});
  };
};

class CreateViewEntry : CreateView {
private:
  Controller *ctrl{nullptr};
  std::string win_title{"Add new Entry"};
  zdb_entry_t new_entry;
  std::vector<cvalue> new_entry_c;

public:
  CreateViewEntry() {
    this->new_entry = {};
    this->new_entry_c = {};
  };

  void set_controller(Controller *ctrl) { this->ctrl = ctrl; };
  void view(LoadedZDB *, bool *);
  void clear() {
    this->new_entry = {};
    this->new_entry_c = {};
  };
};
