#include "settings_view.hpp"
#include "../../../modules/zoomergui/zoomergui.hpp"

void SettingsView::menu() {
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::BeginMenu("Settings")) {
      ImGui::SeparatorText("Launch Panels");
      ImGui::RadioButton("None", &strg->options.launch_panels, launch_panels(none));
      ImGui::SameLine();
      ZoomGui::HelpMarker("Don't load any Table at launch.");
      ImGui::RadioButton("Previous Session", &strg->options.launch_panels, launch_panels(session));
      ImGui::SameLine();
      ZoomGui::HelpMarker("Load previous session Tables at launch.");
      ImGui::RadioButton("All", &strg->options.launch_panels, launch_panels(all));
      ImGui::SameLine();
      ZoomGui::HelpMarker("Load all Tables at launch.");
      ImGui::EndMenu();
    }
    ImGui::EndMainMenuBar();
  }
}
